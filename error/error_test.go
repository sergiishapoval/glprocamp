package error

import (
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type myErrors []struct {
	code        int
	description string
}

func (es myErrors) Error() string {
	if es == nil {
		return "nil"
	}
	res := make([]string, 0, len(es))
	for _, value := range es {
		res = append(res, strconv.Itoa(value.code)+" "+value.description)
	}
	return strings.Join(res, ";")
}

func checkErr(r int) error {
	var es myErrors
	if r > 0 {
		es = append(es, struct {
			code        int
			description string
		}{code: 100, description: "mydesc"})
	} else {
		return nil
	}
	return es
}

func TestError(t *testing.T) {
	err := checkErr(-1)
	if err != nil {
		t.Log("not nil")
	}

	assert.Nil(t, err)

	t.Log(checkErr(2).Error())
}
