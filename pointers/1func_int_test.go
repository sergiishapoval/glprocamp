package pointers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func changeVal(ival int) {
	ival = 0
}

func changePtr(iptr *int) {
	*iptr = 3
}

func TestReferenceVsValueType(t *testing.T) {
	i := 32
	t.Logf("%#v", i)
	pointerI := &i
	t.Logf("%#v", *pointerI)
	var dereferencedPointerI int = *pointerI
	t.Logf("%#v", dereferencedPointerI)

	t.Run("changeVal", func(t *testing.T) {
		changeVal(i)
		assert.Equal(t, 32, i, "i is changed after changeVal")
		assert.Equal(t, 32, *pointerI, "pointerI is changed after changeVal")
	})

	t.Run("changePtr", func(t *testing.T) {
		changePtr(pointerI)
		assert.Equal(t, 3, i, "i is changed after changePtr")
		assert.Equal(t, 3, *pointerI, "pointerI is changed after changePtr")
	})
}
