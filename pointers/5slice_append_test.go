package pointers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func appendSlice(vs *[]string) {
	*vs = append(*vs, "newValue")
}

func TestFuncOnSlice_append(t *testing.T) {

	values := make([]string, 2, 3)
	values[0] = "value1"
	values[1] = "value2"

	t.Logf("before call %#v", values)

	appendSlice(&values)

	t.Logf("after call %#v", values)
	assert.Equal(t, 3, len(values), "new value should be appended")
}
