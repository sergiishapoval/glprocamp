package pointers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func changeSlice(vs []string) {
	if len(vs) > 0 {
		vs[0] = "newValue"
	}
}

func TestFuncOnSlice_change(t *testing.T) {

	values := []string{"value1", "value2"}

	t.Logf("before call %#v", values)

	changeSlice(values)

	t.Logf("after call %#v", values)
	assert.Equal(t, 2, len(values), "len should be as before call")
	assert.EqualValues(t, []string{"newValue", "value2"}, values, "new value should be changed")
}
