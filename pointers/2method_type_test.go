package pointers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type myInt int

func (mi myInt) changeVal() {
	mi = 0
}

func (mi *myInt) changePtr() {
	*mi = 3
}

func TestReferenceVsValueTypeInType(t *testing.T) {
	var i myInt = 32
	t.Logf("%#v", i)
	pointerI := &i
	t.Logf("%#v", *pointerI)

	t.Run("changeVal", func(t *testing.T) {
		i.changeVal()
		assert.Equal(t, myInt(32), i, "i is changed after changeVal")
		var i2 myInt = 32
		assert.Equal(t, i2, *pointerI, "pointerI is changed after changeVal")
	})

	t.Run("changePtr", func(t *testing.T) {
		pointerI.changePtr()
		assert.Equal(t, myInt(3), i, "i is changed after changePtr")
		var i2 myInt = 3
		assert.Equal(t, i2, *pointerI, "pointerI is changed after changePtr")
	})
}
