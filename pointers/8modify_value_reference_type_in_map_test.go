package pointers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type Room struct {
	chairQty int
}

func TestModifyValueInMap(t *testing.T) {
	rooms := map[string]Room{
		"KBP1-1L-TC": {
			chairQty: 4,
		},
	}
	//rooms["KBP1-1L-TC"].chairQty = 5
	assert.Equal(t, 5, rooms["KBP1-1L-TC"].chairQty, "chairQty updated")
}

func TestModifyReferenceInMap(t *testing.T) {
	rooms := map[string]*Room{
		"KBP1-1L-TC": {
			chairQty: 4,
		},
	}
	rooms["KBP1-1L-TC"].chairQty = 5
	assert.Equal(t, 5, rooms["KBP1-1L-TC"].chairQty, "chairQty updated")
}

func TestModifyValueInMap2(t *testing.T) {
	rooms := map[string]Room{
		"KBP1-1L-TC": {
			chairQty: 4,
		},
	}
	glRoom := rooms["KBP1-1L-TC"]
	glRoom.chairQty = 5
	assert.Equal(t, 5, rooms["KBP1-1L-TC"].chairQty, "chairQty updated")
}

type Bank struct {
	ID   int
	Name string
}

type BankAccount struct {
	ID      int
	Balance string
}

type UserBankingData struct {
	Banks        map[int]*Bank
	BankAccounts map[int]*BankAccount
}
