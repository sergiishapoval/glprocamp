package pointers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func changeMap(vs map[string]string) {
	vs["unknownKey"] = "newValue"
}

func TestFuncOnMap(t *testing.T) {

	values := map[string]string{
		"key1": "value1",
		"key2": "value2",
	}
	t.Logf("before call %#v", values)

	changeMap(values)
	v, ok := values["unknownKey"]
	t.Logf("after call %#v", values)
	assert.True(t, ok, "unknownKey should be added")
	assert.Equal(t, "newValue", v, "value should be correct")
}
