package pointers

import (
	"strconv"
	"sync"
	"testing"
)

type storage struct {
	mu       *sync.RWMutex
	products map[string]bool
}

func makeRecord() storage {
	return storage{
		products: make(map[string]bool),
		mu:       &sync.RWMutex{},
	}
}

func (r storage) updateProduct(productName string, hasProduct bool) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.products[productName] = hasProduct
}

func TestRecord_updateProduct(t *testing.T) {
	qty := 200

	r := makeRecord()

	// CountDownLatch to wait all routines are ready
	startFillStorageGroup := sync.WaitGroup{}
	startFillStorageGroup.Add(2 * qty)
	// CountDownLatch to wait all routines are finished
	endFillStorageGroup := sync.WaitGroup{}
	endFillStorageGroup.Add(2 * qty)

	for i := 0; i < 2*qty; i++ {
		go func(position int) {
			startFillStorageGroup.Done()
			startFillStorageGroup.Wait()
			r.updateProduct(strconv.Itoa(position), true)
			endFillStorageGroup.Done()
		}(i)
	}

	endFillStorageGroup.Wait()

	for i := 0; i < 2*qty; i++ {
		if _, ok := r.products[strconv.Itoa(i)]; !ok {
			t.Errorf("%d product expected, but not found", i)
		}
	}
}
