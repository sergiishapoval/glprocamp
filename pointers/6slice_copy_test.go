package pointers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSliceSyntaxCopy(t *testing.T) {
	s := []int{1, 2, 3, 4}
	s2 := s[:]
	s2[0] = 8
	assert.Equal(t, s[0], 8)
}

// check also https://github.com/go101/go101/wiki/How-to-perfectly-clone-a-slice%3F
// and https://github.com/golang/go/wiki/SliceTricks
func TestSlices(t *testing.T) {
	s := []int{1, 2, 3, 4}
	s2 := make([]int, len(s))
	copy(s2, s)
	s2[0] = 8
	assert.Equal(t, s[0], 1)
	assert.Equal(t, s2[0], 8)
}
