package closures

import (
	log "github.com/sirupsen/logrus"
	"sync"
	"testing"
	"time"
)

func TestClosureEasyGotcha(t *testing.T) {
	data := []string{"one", "two", "three"}

	group := sync.WaitGroup{}
	group.Add(len(data))
	for _, v := range data {
		v := v
		// solution 1
		go func() {
			defer group.Done()
			log.Print(v)
			//t.Log(v)
		}() // solution 2
		time.Sleep(2 * time.Second)
	}
	group.Wait()
}
