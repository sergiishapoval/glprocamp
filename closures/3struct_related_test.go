package closures

import (
	"fmt"
	"testing"
	"time"
)

//https://habr.com/ru/company/mailru/blog/314804/#47
//https://habr.com/ru/company/mailru/blog/314804/#48
//http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/
type field struct {
	name string
}

func (p *field) print() {
	fmt.Println(p.name)
}

func TestValueStructRelated(t *testing.T) {
	data := []*field{{"one"}, {"two"}, {"three"}} //solution 1

	for _, v := range data {
		go v.print()
	}

	time.Sleep(3 * time.Second)
}
