package closures

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func AuthorFromEmail(email string) string {
	return strings.Split(email, "@")[0]
}

func TestAuthorFromEmail(t *testing.T) {
	t.Parallel()
	for name, tc := range map[string]struct {
		in     string
		expOut string
	}{
		"not email": {"", ""},
		"email":     {"firstname.lastname@numbrs.com", "firstname.lastname"},
	} {
		tc := tc
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			assert.Equal(t, tc.expOut, AuthorFromEmail(tc.in))
		})
	}
}
