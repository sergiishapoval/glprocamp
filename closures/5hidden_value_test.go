package closures

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func intSeq() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func TestSequenceGenerator(t *testing.T) {
	seq := intSeq()
	assert.Equal(t, 1, seq())
	assert.Equal(t, 2, seq())
	assert.Equal(t, 3, seq())
	seq2 := intSeq()
	assert.Equal(t, 1, seq2())
	assert.Equal(t, 2, seq2())
	assert.Equal(t, 3, seq2())
}

func commonSuffix(suffix string) func(value string) string {
	return func(value string) string {
		return value + suffix
	}
}

func TestCommonSuffix(t *testing.T) {
	smile := commonSuffix("))")
	assert.Equal(t, "start))", smile("start"))
	exclamation := commonSuffix("!!!")
	assert.Equal(t, "hope you are doing well!!!", exclamation("hope you are doing well"))
}
