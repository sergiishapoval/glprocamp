package closures

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLoggingInDefer(t *testing.T) {
	res, e := toValidState(t, 9)
	assert.NoError(t, e)
	assert.Equal(t, 1, res)
}

func toValidState(t *testing.T, value int) (int, error) {
	var (
		res int
		err error
	)

	defer func() {
		t.Logf("res: %d, err: <%v>", res, err)
	}()

	switch {
	case value < 0:
		res, err = -1, nil
	case value == 0:
		res, err = 0, nil
	case value > 0 && value < 10:
		res, err = 1, nil
	default:
		res, err = 0, errors.New("invalid input")
	}
	return res, err
}

func TestLoggingInDeferSimplified(t *testing.T) {
	res, e := toValidStateSimplified(t, -4)
	assert.NoError(t, e)
	assert.Equal(t, -1, res)
}

func toValidStateSimplified(t *testing.T, value int) (res int, err error) {
	defer func() {
		t.Logf("res: %d, err: <%v>", res, err)
	}()
	switch {
	case value < 0:
		return -1, nil
	case value == 0:
		return 0, nil
	case value > 0 && value < 10:
		return 1, nil
	default:
		return 0, errors.New("invalid input")
	}
}
